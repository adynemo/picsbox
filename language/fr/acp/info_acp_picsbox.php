<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, [
	'ACP_PICSBOX_TITLE'                   => 'PicsBox Configuration',
	'ACP_CAT_PICSBOX'                     => 'PicsBox Extension',
	'ACP_PICSBOX_CONFIG'                  => 'Configuration',
	'ACP_PICSBOX_SAVED'                   => 'Modifications sauvegardées.',
	'ACP_PICSBOX_ERROR'                   => 'Une erreur est survenue.',
	'ACP_PICSBOX_EXPLAIN'                 => 'Configure PicsBox.',
	'ACP_PICSBOX_IMAGE_PATH'              => 'Chemin des images',
	'ACP_PICSBOX_IMAGE_PATH_EXPLAIN'      => 'Chemin relatif des images, à partir de la base de phpBB <kbd>/images/picsbox</kbd>.<br>Laissez le champ vide pour garder le chemin par défaut.<br>Défaut : <kbd>%1$s</kbd><br><em>Attention, si vous changez cette valeur, les anciennes images ne seront plus visibles et causeront une 404.</em>',
	'ACP_PICSBOX_UPLOAD_MAX_SIZE'         => 'Poids maximum',
	'ACP_PICSBOX_UPLOAD_MAX_SIZE_EXPLAIN' => 'Poids maximum des images pour le téléchargement en MB.<br>Laissez vide ou 0 pour prendre la valeur par défaut.<br>Défaut : <kbd>%1$s MB</kbd><br><em>Attention, cette valeur dépend également de votre configuration PHP.</em>',
	'ACP_PICSBOX_PURGE_TITLE'             => 'Purge',
	'ACP_PICSBOX_PURGE_LABEL'             => 'Purge',
	'ACP_PICSBOX_PURGE_EXPLAIN'           => 'Purge les images inutilisées du serveur.',
	'ACP_PICSBOX_STATS_TITLE'             => 'Statistiques',
	'ACP_PICSBOX_STATS_COUNT_IMAGES'      => 'Nombre d’images',
	'ACP_PICSBOX_STATS_COUNT_ORPHANS'     => 'Nombre d’images orphelines',
	'ACP_PICSBOX_ORIGINAL_SIZE_LABEL'     => 'Redimensionnement',
	'ACP_PICSBOX_ORIGINAL_SIZE_EXPLAIN'   => 'Autoriser l’utilisateur à ne pas redimensionner l’image (garde l’originale).',
	'LOG_PICSBOX_SETTINGS_UPDATE'         => '<strong>[PicsBox] Mise à jour des paramètres PicsBox</strong><br>» %1$s<br>» Chemin : %2$s<br>» Poids : %3$s kB<br>» Resize: %4$s',
	'LOG_PICSBOX_PURGE'                   => '<strong>[PicsBox] PicsBox a été purgée</strong><br>» Utilisateur : %1$s<br>» BDD : %2$d<br>» Fichiers : %3$d',
	'LOG_PICSBOX_SETTINGS_ERROR'          => '<strong>[PicsBox] Une erreur est survenue lors de la mise à jour des paramètres PicsBox</strong><br>» Utilisateur: %1$s<br>» %2$s: %3$s',
	'LOG_PICSBOX_PURGE_ERROR'             => '<strong>[PicsBox] Une erreur est survenue lors de la purge PicsBox</strong><br>» Utilisateur: %1$s<br>» %2$s: %3$s',
	'LOG_PICSBOX_DELETE_IMAGE'            => '<strong>[PicsBox] Les images suivantes sont supprimées</strong><br>» Noms de fichier : %1$s<br>» Script: %2$s',
]);
