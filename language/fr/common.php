<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, [
	'PICSBOX_TAB_TITLE'                => 'Images | PicsBox',
	'PICSBOX_TAB_COPYRIGHT'            => '<a href="https://gitlab.com/adynemo/picsbox">PicsBox, auto-hébergeur d’images par Ady.</a>',
	'PICSBOX_UPLOAD_ERROR_SIZE_FILE'   => 'Le fichier téléchargé est trop lourd.',
	'PICSBOX_UPLOAD_ERROR_NO_FILE'     => 'Aucun fichier téléchargé.',
	'PICSBOX_UPLOAD_ERROR_MIME_TYPE'   => 'Ce type de fichier n’est pas autorisé. Veuillez télécharger un jpeg ou un png.',
	'PICSBOX_GLOBAL_ERROR'			   => 'Une erreur s’est produite. Réessayez plus tard ou contactez un administrateur.',
	'PICSBOX_CHOOSE_FILE'              => 'Choisissez un fichier',
	'PICSBOX_NO_SELECTED_FILE'         => 'Aucun fichier sélectionné.',
	'PICSBOX_CLIPBOARD'                => 'Copier dans le presse-papiers',
	'PICSBOX_REMOVE_IMAGE_BUTTON'      => 'Supprimer l’image',
	'PICSBOX_REMOVE_IMAGE_INFORMATION' => 'Ceci supprimera totalement l’image, elle ne sera plus accessible. Pour télécharger une nouvelle image, cliquez sur `Choisissez un fichier`.',
	'PICSBOX_RESIZE_EXPLAIN'           => 'Vous pouvez changer le redimensionnement de votre image.',
	'PICSBOX_NO_RESIZE'                => 'Aucun redimensionnement',
	'PICSBOX_SMALL_MODE'               => 'Petit : 100x75px',
	'PICSBOX_THUMBNAIL_MODE'           => 'Vignette : 150x112px',
	'PICSBOX_MEDIUM_MODE'              => 'Moyen : 240x180px',
	'PICSBOX_NORMAL_MODE'              => 'Normal : 320x240px',
	'PICSBOX_FORUM_MODE'               => 'Forum : 640x480px',
]);
