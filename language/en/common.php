<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, [
	'PICSBOX_TAB_TITLE'                => 'Images | PicsBox',
	'PICSBOX_TAB_COPYRIGHT'            => '<a href="https://gitlab.com/adynemo/picsbox">PicsBox, images self-hosted by Ady.</a>',
	'PICSBOX_UPLOAD_ERROR_SIZE_FILE'   => 'The uploaded file is too heavy.',
	'PICSBOX_UPLOAD_ERROR_NO_FILE'     => 'No uploaded file.',
	'PICSBOX_UPLOAD_ERROR_MIME_TYPE'   => 'This type of file is not allowed. Please, upload a jpeg or a png.',
	'PICSBOX_GLOBAL_ERROR'			   => 'An error occurred. Please, retry later or contact an administrator.',
	'PICSBOX_CHOOSE_FILE'              => 'Choose a file',
	'PICSBOX_NO_SELECTED_FILE'         => 'No selected file.',
	'PICSBOX_CLIPBOARD'                => 'Copy to clipboard',
	'PICSBOX_REMOVE_IMAGE_BUTTON'      => 'Delete image',
	'PICSBOX_REMOVE_IMAGE_INFORMATION' => 'This action will remove this image, this one will be no more accessible. To upload a new image, please click on `Choose a file`.',
	'PICSBOX_RESIZE_EXPLAIN'           => 'You can change your image resizing.',
	'PICSBOX_NO_RESIZE'                => 'No resizing',
	'PICSBOX_SMALL_MODE'               => 'Small: 100x75px',
	'PICSBOX_THUMBNAIL_MODE'           => 'Thumbnail: 150x112px',
	'PICSBOX_MEDIUM_MODE'              => 'Medium: 240x180px',
	'PICSBOX_NORMAL_MODE'              => 'Normal: 320x240px',
	'PICSBOX_FORUM_MODE'               => 'Forum: 640x480px',
]);
