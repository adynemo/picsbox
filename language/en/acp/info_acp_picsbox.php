<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, [
	'ACP_PICSBOX_TITLE'                   => 'PicsBox Configuration',
	'ACP_CAT_PICSBOX'                     => 'PicsBox Extension',
	'ACP_PICSBOX_CONFIG'                  => 'Configuration',
	'ACP_PICSBOX_SAVED'                   => 'Changes Saved.',
	'ACP_PICSBOX_ERROR'                   => 'Error occurred.',
	'ACP_PICSBOX_EXPLAIN'                 => 'Configure PicsBox.',
	'ACP_PICSBOX_IMAGE_PATH'              => 'Images path',
	'ACP_PICSBOX_IMAGE_PATH_EXPLAIN'      => 'Relative path to images from the phpBB root <kbd>/images/picsbox</kbd>.<br>Keep it empty to take the default path.<br>Default: <kbd>%1$s</kbd><br>Attention, if you change this value, oldest images will not be displayed anymore and will cause 404.',
	'ACP_PICSBOX_UPLOAD_MAX_SIZE'         => 'Max upload size',
	'ACP_PICSBOX_UPLOAD_MAX_SIZE_EXPLAIN' => 'Max upload size allowed in MB.<br>Keep it empty or 0 to take the default value.<br>Default: <kbd>%1$s MB</kbd><br><em>Attention, this value also depends on your PHP configuration.</em>',
	'ACP_PICSBOX_PURGE_TITLE'             => 'Purge',
	'ACP_PICSBOX_PURGE_LABEL'             => 'Purge',
	'ACP_PICSBOX_PURGE_EXPLAIN'           => 'Purge unused images from server.',
	'ACP_PICSBOX_STATS_TITLE'             => 'Stats',
	'ACP_PICSBOX_STATS_COUNT_IMAGES'      => 'Images count',
	'ACP_PICSBOX_STATS_COUNT_ORPHANS'     => 'Orphan images count',
	'ACP_PICSBOX_ORIGINAL_SIZE_LABEL'     => 'Resize',
	'ACP_PICSBOX_ORIGINAL_SIZE_EXPLAIN'   => 'Allow the user to upload an image without resizing it (keep the original).',
	'LOG_PICSBOX_SETTINGS_UPDATE'         => '<strong>[PicsBox] PicsBox settings are updated</strong><br>» %1$s<br>» Path: %2$s<br>» Size: %3$s kB<br>» Resize: %4$s',
	'LOG_PICSBOX_PURGE'                   => '<strong>[PicsBox] PicsBox purge processed</strong><br>» User: %1$s<br>» DB: %2$d<br>» Files: %3$d',
	'LOG_PICSBOX_SETTINGS_ERROR'          => '<strong>[PicsBox] PicsBox settings, error occurred</strong><br>» Username: %1$s<br>» %2$s: %3$s',
	'LOG_PICSBOX_PURGE_ERROR'             => '<strong>[PicsBox] PicsBox purge, error occurred</strong><br>» Username: %1$s<br>» %2$s: %3$s',
	'LOG_PICSBOX_DELETE_IMAGE'            => '<strong>[PicsBox] The following images are deleted</strong><br>» Filenames: %1$s',
]);
