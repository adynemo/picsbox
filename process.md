## Images process

### Creation

1. Handle news

New images have been stored in database, during upload, with post id at 0.

**Action:** Database's entries will be updated with the new post id.

2. Handle additions from another post

The filename exists in database but with another post id.

**Action:** New database's entry will be created with this filename and this post id.

### Edition

1. Handle news

New images have been stored in database, during upload, with post id at 0.

**Action:** Database's entries will be updated with the new post id.

2. Handle removals

It's the diff between filenames that already exist for this post in database, and matched filenames in post's text.

**Action:** Remove from DB and check if this filename exists for another post

- true: do nothing
- false: remove from filesystem

3. Handle additions from another post

The filename exists in database but with another post id.

**Action:** New database's entry will be created with this filename and this post id.
