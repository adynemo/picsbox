<?php

namespace ady\picsbox\object;

class settings
{
	/**
	 * @var string
	 */
	private $image_path;
	/**
	 * @var int
	 */
	private $max_upload_size;
	/**
	 * @var int
	 */
	private $allow_original_size;

	public function get_image_path(): string
	{
		return $this->image_path;
	}

	public function set_image_path(string $image_path): settings
	{
		$this->image_path = $image_path;

		return $this;
	}

	public function get_max_upload_size(): int
	{
		return $this->max_upload_size;
	}

	public function set_max_upload_size(int $max_upload_size): settings
	{
		$this->max_upload_size = $max_upload_size;

		return $this;
	}

	public function get_allow_original_size(): int
	{
		return $this->allow_original_size;
	}

	public function set_allow_original_size(int $allow_original_size): settings
	{
		$this->allow_original_size = $allow_original_size;

		return $this;
	}
}
