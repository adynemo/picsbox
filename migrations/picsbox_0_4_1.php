<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\migrations;

use ady\picsbox\constant\config;

class picsbox_0_4_1 extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return [
			['config.add', [config::UPLOAD_MAX_SIZE_NAME, 0]],
		];
	}
}
