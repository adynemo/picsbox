<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\migrations;

class picsbox_0_4_0 extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return [
			'add_tables' => [
				$this->table_prefix . 'picsbox' => [
					'COLUMNS'     => [
						'id'         => ['UINT', null, 'auto_increment'],
						'filename'   => ['VCHAR:41', ''],
						'post_id'    => ['UINT:10', 0],
						'user_id'    => ['UINT:10', 0],
						'created_at' => ['UINT:11', 0],
					],
					'PRIMARY_KEY' => 'id',
				],
			],
		];
	}

	public function revert_schema()
	{
		return [
			'drop_tables' => [
				$this->table_prefix . 'picsbox',
			],
		];
	}
}
