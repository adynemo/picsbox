<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\migrations;

use ady\picsbox\constant\config;

class picsbox_0_0_1 extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return [
			['config.add', [config::IMAGE_PATH_NAME, '']],
			['module.add', [
				'acp',
				'ACP_CAT_DOT_MODS',
				'ACP_CAT_PICSBOX',
			]],
			['module.add', [
				'acp',
				'ACP_CAT_PICSBOX',
				[
					'module_basename' => '\ady\picsbox\acp\acp_picsbox_module',
					'modes'           => ['settings'],
				],
			]],
		];
	}
}
