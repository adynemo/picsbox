<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\constant;


class size
{
	const SMALL_MODE = 'small';
	const THUMBNAIL_MODE = 'thumbnail';
	const MEDIUM_MODE = 'medium';
	const NORMAL_MODE = 'normal';
	const FORUM_MODE = 'forum';

	const MODES = [
		self::SMALL_MODE,
		self::THUMBNAIL_MODE,
		self::MEDIUM_MODE,
		self::NORMAL_MODE,
		self::FORUM_MODE,
	];
	const SMALL = [
		'height' => 100,
		'width'  => 75,
	];
	const THUMBNAIL = [
		'height' => 150,
		'width'  => 112,
	];
	const MEDIUM = [
		'height' => 240,
		'width'  => 180,
	];
	const NORMAL = [
		'height' => 320,
		'width'  => 240,
	];
	const FORUM = [
		'height' => 640,
		'width'  => 480,
	];
}
