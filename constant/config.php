<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\constant;

class config
{
	const IMAGE_PATH_NAME = 'picsbox_image_path';
	const DEFAULT_IMAGE_PATH = '/images/ady/picsbox';
	const UPLOAD_MAX_SIZE_NAME = 'picsbox_max_upload_size';
	const DEFAULT_UPLOAD_MAX_SIZE = 5; // in megabytes
	const PICSBOX_ICON = '/ext/ady/picsbox/styles/all/theme/picsbox_icon.png';
	const ALLOW_ORIGINAL_SIZE_NAME = 'picsbox_allow_original_size';
}
