<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\constant;


class mimeTypes
{
	const JPEG = 'image/jpeg';
	const PNG = 'image/png';

	const MIME_TYPES_MAP = [
		self::JPEG => 'jpeg',
		self::PNG  => 'png',
	];
}
