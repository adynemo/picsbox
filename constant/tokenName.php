<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\constant;

class tokenName
{
	const POST = 'picsbox-post';
	const DELETE = 'picsbox-delete';
}
