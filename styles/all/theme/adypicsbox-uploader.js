(function() {
	const AdyPicsBox = {
		uploader: () => {
			const mimeTypes = ['image/jpeg', 'image/png'];
			const submitButton = document.querySelector('#picsbox-post-image .submit-buttons input');
			const loader = document.querySelector('#picsbox-post-image .picsbox-loader');

			submitButton.addEventListener('click', function() {
				AdyPicsBox.enableLoader(loader, submitButton);
				const selectedFile = document.querySelector('#picsbox-upload');
				const noFileError = selectedFile.dataset.picsboxNoFileError;
				const maxUploadSize = selectedFile.dataset.picsboxMaxUploadSize * Math.pow(10, 6);
				const maxUploadSizeError = selectedFile.dataset.picsboxMaxUploadSizeError;
				const mimeTypeError = selectedFile.dataset.picsboxMimeTypeError;
				const globalError = selectedFile.dataset.picsboxGlobalError;
				const tokens = JSON.parse(selectedFile.dataset.picsboxTokens);
				if (selectedFile.files.length === 0) {
					alert(noFileError);
					AdyPicsBox.disableLoader(loader, submitButton);
					return;
				}

				const file = selectedFile.files[0];

				if (mimeTypes.indexOf(file.type) === -1) {
					alert(mimeTypeError);
					AdyPicsBox.disableLoader(loader, submitButton);
					return;
				}

				if (file.size > maxUploadSize) {
					alert(maxUploadSizeError);
					AdyPicsBox.disableLoader(loader, submitButton);
					return;
				}

				const formData = new FormData();
				const request = new XMLHttpRequest();

				formData.append('file', document.querySelector('#picsbox-upload').files[0]);
				formData.append('resize-mode', AdyPicsBox.getResizeOption());
				formData.append('creation_time', tokens.creation_time);
				formData.append('form_token', tokens.post_token);

				request.onreadystatechange = function() {
					if (request.readyState === XMLHttpRequest.DONE) {
						const response = JSON.parse(request.responseText);
						if (undefined !== response.url) {
							const url = response.url;
							const container = document.getElementById('picsbox-result');
							const image = document.getElementById('picsbox-preview-image');
							const bbcodeInput = document.getElementById('picsbox-bbcode-image');
							const removeButton = document.getElementById('picsbox-remove-image');

							image.src = url;
							bbcodeInput.value = `[img]${url}[/img]`;
							removeButton.dataset.filename = response.filename;

							container.style.display = 'initial';
						} else {
							alert(globalError);
						}
						AdyPicsBox.disableLoader(loader, submitButton);
					}
				}

				const url = document.getElementById('picsbox-post-image').dataset.picsboxApiPost;
				request.open('post', url);
				request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
				request.send(formData);
			});
		},
		customizeUpload: () => {
			const button = document.getElementById('picsbox-upload');
			const filenameContainer = document.getElementById('file-chosen');

			button.addEventListener('change', function() {
				filenameContainer.textContent = this.files[0].name
			})
		},
		copyToClipboard: () => {
			const button = document.getElementById('picsbox-copy')
			const target = document.getElementById('picsbox-bbcode-image')

			button.onclick = () => {
				target.select();
				document.execCommand('copy');
			}
		},
		removeImage: () => {
			const button = document.getElementById('picsbox-remove-image');
			const selectedFile = document.querySelector('#picsbox-upload');
			const tokens = JSON.parse(selectedFile.dataset.picsboxTokens);

			button.addEventListener('click', function() {
				if (confirm(button.dataset.information)) {
					const url = `${button.dataset.picsboxApiDelete}/${button.dataset.filename}`;
					const request = new XMLHttpRequest();
					const formData = new FormData();

					formData.append('creation_time', tokens.creation_time);
					formData.append('form_token', tokens.delete_token);

					request.onreadystatechange = function() {
						if (request.readyState === XMLHttpRequest.DONE) {
							if (202 === request.status) {
								const container = document.getElementById('picsbox-result');
								const input = document.getElementById('picsbox-upload');
								const filenameContainer = document.getElementById('file-chosen');

								input.value = '';
								filenameContainer.textContent = filenameContainer.dataset.textOrigin;
								container.style.display = 'none';
							}
						}
					}

					request.open('post', url);
					request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					request.send(formData);
				}
			});
		},
		getResizeOption: () => {
			const select = document.getElementById('picsbox-resize-option');
			const options = select.getElementsByTagName('option');

			for (const option of options) {
				if (true === option.selected) {
					return option.value;
				}
			}
		},
		enableLoader: (loader, submitButton) => {
			loader.classList.add('active');
			submitButton.classList.add('disabled');
		},
		disableLoader: (loader, submitButton) => {
			loader.classList.remove('active');
			submitButton.classList.remove('disabled');
		}
	};

	AdyPicsBox.customizeUpload();
	AdyPicsBox.uploader();
	AdyPicsBox.copyToClipboard();
	AdyPicsBox.removeImage();
})()
