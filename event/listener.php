<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\event;

use ady\picsbox\constant\config;
use ady\picsbox\constant\tokenName;
use ady\picsbox\core\repository;
use ady\picsbox\core\stringManager;
use ady\picsbox\core\uploader;
use phpbb\config\db;
use phpbb\controller\helper;
use phpbb\event\data;
use phpbb\log\log;
use phpbb\user;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener
 */
class listener implements EventSubscriberInterface
{
	/**
	 * @var helper
	 */
	private $helper;
	/**
	 * @var db
	 */
	private $config;
	/**
	 * @var repository
	 */
	private $repository;
	/**
	 * @var stringManager
	 */
	private $stringManager;
	/**
	 * @var uploader
	 */
	private $uploader;
	/**
	 * @var user
	 */
	private $user;
	/**
	 * @var log
	 */
	private $log;

	public function __construct(
		helper $helper,
		db $config,
		repository $repository,
		stringManager $stringManager,
		uploader $uploader,
		user $user,
		log $log
	)
	{
		$this->helper = $helper;
		$this->config = $config;
		$this->repository = $repository;
		$this->stringManager = $stringManager;
		$this->uploader = $uploader;
		$this->user = $user;
		$this->log = $log;
	}

	/**
	 * Assign functions defined in this class to event listeners in the core
	 *
	 * @return array
	 * @static
	 * @access public
	 */
	public static function getSubscribedEvents()
	{
		return [
			'core.user_setup'                   => 'load_language_on_setup',
			'core.posting_modify_template_vars' => 'give_variables_to_posting_template',
			'core.submit_post_end'              => 'store_images',
		];
	}

	public function load_language_on_setup(data $event): void
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = [
			'ext_name' => 'ady/picsbox',
			'lang_set' => 'common',
		];
		$event['lang_set_ext'] = $lang_set_ext;
	}

	public function give_variables_to_posting_template(data $event): void
	{
		$tokens = $this->getTokens();

		$allow_original_size = filter_var($this->config->offsetGet(config::ALLOW_ORIGINAL_SIZE_NAME), FILTER_VALIDATE_BOOLEAN);
		$routes = [
			'ADY_PICSBOX_IS_PRESENT'          => true,
			'ADY_PICSBOX_API_POST_IMAGE'      => $this->helper->route('ady_picsbox_api_image_post'),
			'ADY_PICSBOX_API_DELETE_IMAGE'    => $this->helper->route('ady_picsbox_api_image_delete'),
			'ADY_PICSBOX_ICON_ROUTE'          => generate_board_url() . config::PICSBOX_ICON,
			'ADY_PICSBOX_MAX_UPLOAD_SIZE'     => $this->config->offsetGet(config::UPLOAD_MAX_SIZE_NAME) ?: config::DEFAULT_UPLOAD_MAX_SIZE,
			'ADY_PICSBOX_ALLOW_ORIGINAL_SIZE' => $allow_original_size,
			'ADY_PICSBOX_TOKENS'              => $tokens,
		];

		$event['page_data'] = array_merge($event['page_data'], $routes);
	}

	public function store_images(data $event): void
	{
		$mode = $event['mode'];
		$post_id = $event['data']['post_id'];
		$poster_id = $event['data']['poster_id'];
		$message = $event['data']['message'];

		$filenames = $this->stringManager->search_filenames($message);
		$filenames = array_unique($filenames);

		try
		{
			$already_exist = $this->repository->fetch_by_post($post_id);
			if ([] !== $filenames)
			{
				$not_exist_yet = array_diff($filenames, $already_exist);
				$news = $this->handle_news($not_exist_yet, $post_id);

				if ('edit' === $mode)
				{
					$removals = array_diff($already_exist, $filenames);
					$this->handle_removals($removals);
				}

				$additions = array_diff($not_exist_yet, $news);
				$this->handle_additions($additions, $post_id, $poster_id);
			}
			else
			{
				if ('edit' === $mode)
				{
					$this->handle_removals($already_exist);
				}
			}
		}
		catch (\Throwable $exception)
		{
			trigger_error($exception->getMessage() . 'Post ID: ' . $post_id . 'Data: ' . json_encode($filenames), E_USER_WARNING);
		}
	}

	private function handle_news(array $not_exist_yet, int $post_id): array
	{
		$news = $this->repository->exist($not_exist_yet, 0);

		if ([] !== $news)
		{
			$results = $this->repository->update_orphans($news, $post_id);
			if ($results !== count($news))
			{
				trigger_error('PicsBox: Some new images have not been updated in the database. Data: ' . json_encode($news), E_USER_WARNING);
			}
		}

		return $news;
	}

	private function handle_removals(array $removals): void
	{
		if ([] !== $removals)
		{
			$removed = $this->repository->multi_remove(array_keys($removals));
			if ($removed !== count($removals))
			{
				trigger_error('PicsBox: Some orphan images are not removed from the database. Data: ' . json_encode($removals), E_USER_WARNING);
			}

			$on_another_post = $this->repository->exist($removals, null);
			$removals = array_diff($removals, $on_another_post);
			$this->log->add(
				'user',
				$this->user->data['user_id'],
				$this->user->ip,
				'LOG_PICSBOX_DELETE_IMAGE',
				false,
				[implode('; ', $removals), 'post']
			);
			foreach ($removals as $item)
			{
				$this->uploader->remove($item);
			}
		}
	}

	private function handle_additions(array $additions, int $post_id, int $poster_id): void
	{
		if ([] !== $additions)
		{
			$entries = [];
			foreach ($additions as $filename)
			{
				$entries[] = $this->repository->prepare($filename, $post_id, $poster_id);
			}
			$this->repository->store($entries);
		}
	}

	/**
	 * @return false|string
	 */
	public function getTokens()
	{
		$now = time();
		$token_sid = ($this->user->data['user_id'] == ANONYMOUS && !empty($this->config['form_token_sid_guests'])) ? $this->user->session_id : '';
		$postToken = sha1($now . $this->user->data['user_form_salt'] . tokenName::POST . $token_sid);
		$deleteToken = sha1($now . $this->user->data['user_form_salt'] . tokenName::DELETE . $token_sid);

		return json_encode([
			'creation_time' => $now,
			'post_token'    => $postToken,
			'delete_token'  => $deleteToken,
		]);
	}
}
