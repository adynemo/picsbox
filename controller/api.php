<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\controller;

use ady\picsbox\constant\http;
use ady\picsbox\constant\size;
use ady\picsbox\constant\tokenName;
use ady\picsbox\core\pathManager;
use ady\picsbox\core\repository;
use ady\picsbox\core\resizer;
use ady\picsbox\core\uploader;
use phpbb\exception\http_exception;
use phpbb\language\language;
use phpbb\log\log;
use phpbb\request\request;
use phpbb\user;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class api
{
	/**
	 * @var user
	 */
	protected $user;
	/**
	 * @var request
	 */
	protected $request;
	/**
	 * @var language
	 */
	protected $lang;
	/**
	 * @var uploader
	 */
	private $uploader;
	/**
	 * @var pathManager
	 */
	private $pathManager;
	/**
	 * @var resizer
	 */
	private $resizer;
	/**
	 * @var repository
	 */
	private $repository;
	/**
	 * @var log
	 */
	private $log;

	public function __construct(
		user $user,
		request $request,
		language $lang,
		uploader $uploader,
		pathManager $pathManager,
		resizer $resizer,
		repository $repository,
		log $log
	)
	{
		$this->user = $user;
		$this->request = $request;
		$this->lang = $lang;
		$this->uploader = $uploader;
		$this->pathManager = $pathManager;
		$this->resizer = $resizer;
		$this->log = $log;
		$this->repository = $repository;
	}

	public function post(): JsonResponse
	{
		$this->allow_request(http::POST);

		$file = $this->request->file('file');
		$resize_mode = $this->request->variable('resize-mode', '');

		try
		{
			$filename = $this->uploader->upload($file);
			$filepath = $this->pathManager->get_image_upload_path($filename);

			$entry = $this->repository->prepare($filename, null, $this->user->data['user_id']);
			$this->repository->store([$entry]);

			if (in_array($resize_mode, size::MODES))
			{
				$this->resizer->resize($filepath, $file['type'], $resize_mode);
			}
		}
		catch (\Throwable $exception)
		{
			trigger_error($this->lang->lang($exception->getMessage()), E_USER_WARNING);

			return new JsonResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		$response = [
			'filename' => $filename,
			'url'      => generate_board_url() . $this->pathManager->get_image_path($filename),
		];

		return new JsonResponse($response, Response::HTTP_CREATED);
	}

	public function delete(string $filename): JsonResponse
	{
		$this->allow_request(http::DELETE);

		if ('' !== $filename)
		{
			try
			{
				$this->log->add(
					'user',
					$this->user->data['user_id'],
					$this->user->ip,
					'LOG_PICSBOX_DELETE_IMAGE',
					false,
					[$filename, 'api']
				);
				$this->uploader->remove($filename);
				$this->repository->remove_orphan($filename);
			}
			catch (\Throwable $exception)
			{
				trigger_error($this->lang->lang($exception->getMessage()), E_USER_WARNING);

				return new JsonResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
			}
		}

		return new JsonResponse(null, Response::HTTP_ACCEPTED);
	}

	private function allow_request(string $action): void
	{
		if (!$this->user->data['is_registered'])
		{
			throw new http_exception(403, 'NOT_AUTHORISED');
		}
		if (!$this->request->is_ajax())
		{
			throw new http_exception(400, 'BAD_REQUEST');
		}
		if (http::POST !== $this->request->server('REQUEST_METHOD'))
		{
			throw new http_exception(405, 'METHOD_NOT_ALLOWED');
		}
		if (http::POST === $action && false === check_form_key(tokenName::POST))
		{
			throw new http_exception(400, 'BAD_REQUEST');
		}
		else if (http::DELETE === $action && false === check_form_key(tokenName::DELETE))
		{
			throw new http_exception(400, 'BAD_REQUEST');
		}
	}
}
