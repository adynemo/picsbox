<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\core;

use ady\picsbox\constant\config;
use ady\picsbox\constant\mimeTypes;
use phpbb\config\db;
use phpbb\filesystem\filesystem;

class uploader
{
	const ERROR_SIZE_FILE = 'PICSBOX_UPLOAD_ERROR_SIZE_FILE';
	const ERROR_NO_FILE = 'PICSBOX_UPLOAD_ERROR_NO_FILE';
	const ERROR_MIME_TYPE = 'PICSBOX_UPLOAD_ERROR_MIME_TYPE';

	/**
	 * @var filesystem
	 */
	private $filesystem;
	/**
	 * @var pathManager
	 */
	private $path_manager;
	/**
	 * @var uuidGenerator
	 */
	private $uuid_generator;
	/**
	 * @var db
	 */
	private $config;
	/**
	 * @var int|string
	 */
	private $max_upload_size;

	public function __construct(
		filesystem $filesystem,
		pathManager $path_manager,
		uuidGenerator $uuid_generator,
		db $config
	)
	{
		$this->filesystem = $filesystem;
		$this->path_manager = $path_manager;
		$this->uuid_generator = $uuid_generator;
		$this->config = $config;
		$this->set_max_upload_size();
	}

	public function upload(array $file): string
	{
		try
		{
			$this->check_image($file);
			$filename = $this->generate_image_filename($file['type']);
			$filepath = $this->path_manager->get_image_upload_path($filename);
			$this->filesystem->copy($file['tmp_name'], $filepath);
			$this->generate_index_htm_for_file($filename);
		}
		catch (\Throwable $exception)
		{
			throw new \Exception($exception->getMessage());
		}

		return $filename;
	}

	public function remove(string $filename): void
	{
		$filepath = $this->path_manager->get_image_upload_path($filename);

		if (true === $this->filesystem->exists($filepath))
		{
			try
			{
				$this->filesystem->remove($filepath);
			}
			catch (\Throwable $exception)
			{
				throw new \Exception($exception->getMessage());
			}
		}
	}

	public function create_images_directory(string $path): void
	{
		$full_path = $this->path_manager->generate_images_directory_path($path);

		try
		{
			$this->filesystem->mkdir($full_path);
			$this->generate_index_htm_directory('');
		}
		catch (\Throwable $exception)
		{
			throw new \Exception($exception->getMessage());
		}
	}

	private function check_image(array $file): void
	{
		if ($file['error'] > 0)
		{
			throw new \Exception(self::ERROR_NO_FILE);
		}
		elseif ($file['size'] > (int) $this->max_upload_size)
		{
			throw new \Exception(self::ERROR_SIZE_FILE);
		}
		elseif (!isset(mimeTypes::MIME_TYPES_MAP[$file['type']]))
		{
			throw new \Exception(self::ERROR_MIME_TYPE);
		}
	}

	private function generate_image_filename(string $mime_type): string
	{
		return sprintf('%s.%s', $this->uuid_generator->generate_v4(), mimeTypes::MIME_TYPES_MAP[$mime_type]);
	}

	private function generate_index_htm_for_file(string $filename): void
	{
		$directories = $this->path_manager->get_directories_path($filename);

		$path = '';
		foreach ($directories as $directory)
		{
			$path .= '/' . $directory;
			$this->generate_index_htm_directory($path);
		}
	}

	private function generate_index_htm_directory(string $directory): void
	{
		$filepath = $this->path_manager->generate_index_htm_path($directory);
		$this->filesystem->dump_file($filepath, self::INDEX_HTM_TEMPLATE);
	}

	private function set_max_upload_size(): void
	{
		$registered = $this->config->offsetGet(config::UPLOAD_MAX_SIZE_NAME);
		$mb = $registered ?: config::DEFAULT_UPLOAD_MAX_SIZE;
		$this->max_upload_size = $mb * pow(10, 6);
	}

	const INDEX_HTM_TEMPLATE = <<<EOT
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style>
body{background-color:#FFFFFF;color:#000000;}
</style>
</head>
<body></body>
</html>

EOT;
}
