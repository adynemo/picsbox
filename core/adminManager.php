<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\core;


use ady\picsbox\constant\config;
use ady\picsbox\object\settings;
use phpbb\config\db;
use phpbb\log\log;
use phpbb\user;

class adminManager
{
	/**
	 * @var repository
	 */
	private $repository;
	/**
	 * @var uploader
	 */
	private $uploader;
	/**
	 * @var db
	 */
	private $config;
	/**
	 * @var user
	 */
	private $user;
	/**
	 * @var log
	 */
	private $log;

	public function __construct(
		repository $repository,
		uploader $uploader,
		db $config,
		user $user,
		log $log
	)
	{
		$this->repository = $repository;
		$this->uploader = $uploader;
		$this->config = $config;
		$this->user = $user;
		$this->log = $log;
	}

	public function processGeneralSettings(settings $settings): bool
	{
		try
		{
			$this->uploader->create_images_directory($settings->get_image_path());
			$this->config->set(config::IMAGE_PATH_NAME, $settings->get_image_path());
			$this->config->set(config::UPLOAD_MAX_SIZE_NAME, $settings->get_max_upload_size());
			$this->config->set(config::ALLOW_ORIGINAL_SIZE_NAME, $settings->get_allow_original_size());
			$this->log('admin', 'LOG_PICSBOX_SETTINGS_UPDATE', [
				$settings->get_image_path(),
				$settings->get_max_upload_size(),
				$settings->get_allow_original_size(),
			]);

			return true;
		}
		catch (\Throwable $exception)
		{
			$this->log('critical', 'LOG_PICSBOX_SETTINGS_ERROR', [get_class($exception), $exception->getMessage()]);

			return false;
		}
	}

	public function processImagesPurge(): bool
	{
		try
		{
			$orphans = $this->repository->fetch_orphans();

			if ([] === $orphans)
			{
				return true;
			}

			$count_db = $this->repository->multi_remove(array_keys($orphans));
			$on_another_post = $this->repository->exist($orphans, null);
			$orphans = array_diff($orphans, $on_another_post);

			$this->log->add(
				'user',
				$this->user->data['user_id'],
				$this->user->ip,
				'LOG_PICSBOX_DELETE_IMAGE',
				false,
				[implode('; ', $orphans), 'purge']
			);
			$count_fs = 0;
			foreach ($orphans as $filename)
			{
				$this->uploader->remove($filename);
				$count_fs++;
			}

			$this->log('admin', 'LOG_PICSBOX_PURGE', [$count_db, $count_fs]);

			return true;
		}
		catch (\Throwable $exception)
		{
			$this->log('critical', 'LOG_PICSBOX_PURGE_ERROR', [get_class($exception), $exception->getMessage()]);

			return false;
		}
	}

	public function calculateStats(): array
	{
		$orphans = $this->repository->fetch_orphans();

		return [
			'count'   => $this->repository->count(),
			'orphans' => count($orphans),
		];
	}

	private function log(string $mode, string $log_key, array $additional_data): void
	{
		$additional_data = array_merge([$this->user->data['username']], $additional_data);
		$this->log->add($mode, $this->user->data['user_id'], $this->user->ip, $log_key, false, $additional_data);
	}
}
