<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\core;


use ady\picsbox\constant\mimeTypes;
use ady\picsbox\constant\size;

class resizer
{
	public function resize(string $filename, string $mime_type, string $mode): bool
	{
		if (!in_array($mode, size::MODES))
		{
			return false;
		}
		if (!([$srcWidth, $srcHeight] = @getimagesize($filename)))
		{
			return false;
		}

		$size = $this->get_size_by_mode($mode);

		if ($srcWidth <= $srcHeight)
		{
			[$height, $width] = $this->calculate_size($size['height'], $srcWidth, $srcHeight);
		}
		else
		{
			[$width, $height] = $this->calculate_size($size['width'], $srcHeight, $srcWidth);
		}

		$image = imagecreatetruecolor($width, $height);
        if (mimeTypes::PNG === $mime_type)
        {
            imagealphablending($image, false);
            imagesavealpha($image, true);
        }
		$srcImage = imagecreatefromstring(file_get_contents($filename));
		imagecopyresampled($image, $srcImage, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);
		imagedestroy($srcImage);

		$result = (mimeTypes::PNG === $mime_type) ? imagepng($image, $filename, 9) : imagejpeg($image, $filename, 90);
		imagedestroy($image);

		return $result;
	}

	private function get_size_by_mode(string $mode): array
	{
		switch ($mode)
		{
			case size::SMALL_MODE:
				return size::SMALL;
			case size::THUMBNAIL_MODE:
				return size::THUMBNAIL;
			case size::MEDIUM_MODE:
				return size::MEDIUM;
			case size::FORUM_MODE:
				return size::FORUM;
			case size::NORMAL_MODE:
			default:
				return size::NORMAL;
		}
	}

	private function calculate_size(int $targetSize, int $srcSide, int $srcOtherSide): array
	{
		return [
			$targetSize,
			($targetSize * $srcSide) / $srcOtherSide,
		];
	}
}
