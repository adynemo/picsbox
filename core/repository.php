<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\core;


use phpbb\db\driver\driver_interface;

class repository
{
	const TABLE = 'picsbox';

	/**
	 * @var driver_interface
	 */
	private $db;
	/**
	 * @var string
	 */
	private $table_name;

	public function __construct(driver_interface $db, string $table_prefix)
	{
		$this->db = $db;
		$this->table_name = $table_prefix . self::TABLE;
	}

	public function prepare(string $filename, ?int $post_id = null, ?int $user_id = null): array
	{
		return [
			'filename'   => $filename,
			'post_id'    => $post_id ?? 0,
			'user_id'    => $user_id ?? 0,
			'created_at' => $this->now(),
		];
	}

	public function store(array $entries): void
	{
		if (!$this->db->sql_multi_insert($this->table_name, $entries))
		{
			throw new \RuntimeException('Store image fails. Data: ' . json_encode($entries));
		}
	}

	public function multi_remove(array $ids): int
	{
		if ([] === $ids)
		{
			return 0;
		}

		$where_clause = sprintf('id IN (%s)', implode(',', $ids));

		return $this->delete($where_clause);
	}

	public function remove_orphan(string $filename): int
	{
		$where_clause = sprintf("filename = '%s' and post_id IS NULL", $filename);

		return $this->delete($where_clause);
	}

	public function fetch_orphans(): array
	{
		$orphans = [];

		$sql_array = [
			'SELECT'    => 'pp.id, pp.filename',
			'FROM'      => [$this->table_name => 'pp'],
			'LEFT_JOIN' => [
				[
					'FROM' => [POSTS_TABLE => 'pp2'],
					'ON'   => 'pp.post_id = pp2.post_id',
				],
			],
			'WHERE'     => 'pp2.post_id IS NULL OR pp.post_id IS NULL',
		];
		$sql = $this->db->sql_build_query('SELECT', $sql_array);

		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return $orphans;
		}

		while ($row = $this->db->sql_fetchrow($result))
		{
			$orphans[$row['id']] = $row['filename'];
		}
		$this->db->sql_freeresult($result);

		return $orphans;
	}

	public function count(): int
	{
		$sql = "SELECT COUNT(id) count FROM $this->table_name";

		$result = $this->db->sql_query($sql);
		$count = $this->db->sql_fetchfield('count');
		$this->db->sql_freeresult($result);

		return $count;
	}

	public function exist(array $filenames, ?int $post_id): array
	{
		$exist = [];
		if ([] === $filenames)
		{
			return $exist;
		}

		$where_clause = sprintf(
			'WHERE filename IN (%s)',
			$this->prepare_in_clause($filenames)
		);

		if (null !== $post_id)
		{
			$where_clause .= ' and post_id = ' . $post_id;
		}

		$sql = sprintf('SELECT filename FROM %s %s', $this->table_name, $where_clause);

		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			$this->db->sql_freeresult($result);
			return $exist;
		}

		while ($row = $this->db->sql_fetchrow($result))
		{
			$exist[] = $row['filename'];
		}
		$this->db->sql_freeresult($result);

		return $exist;
	}

	public function fetch_by_post(int $post_id): array
	{
		$filenames = [];

		$sql = 'SELECT id, filename FROM ' . $this->table_name . ' WHERE ' . $this->db->sql_build_array('SELECT', ['post_id' => $post_id]);
		$result = $this->db->sql_query($sql);

		if (!$result)
		{
			$this->db->sql_freeresult($result);
			return $filenames;
		}

		while ($row = $this->db->sql_fetchrow($result))
		{
			$filenames[$row['id']] = $row['filename'];
		}
		$this->db->sql_freeresult($result);

		return $filenames;
	}

	public function update_orphans(array $filenames, int $post_id): int
	{
		if ([] === $filenames)
		{
			return 0;
		}

		$sql = sprintf(
			'UPDATE %s SET %s WHERE post_id = 0 AND filename IN (%s)',
			$this->table_name,
			$this->db->sql_build_array('UPDATE', ['post_id' => $post_id]),
			$this->prepare_in_clause($filenames)
		);

		$this->db->sql_query($sql);

		return $this->db->sql_affectedrows();
	}

	private function prepare_in_clause(array $items): string
	{
		$format = '"%s"';
		$list = '';

		foreach ($items as $item)
		{
			$list .= sprintf($format, $item) . ',';
		}

		return rtrim($list, ',');
	}

	private function delete(string $where_clause): int
	{
		$sql = sprintf('DELETE FROM %s WHERE %s', $this->table_name, $where_clause);
		$this->db->sql_query($sql);

		return $this->db->sql_affectedrows();
	}

	private function now(): int
	{
		return (new \DateTime('now'))->getTimestamp();
	}
}
