<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\core;

use ady\picsbox\constant\config;
use phpbb\config\db;

class pathManager
{
	const INDEX_HTM_FILENAME = 'index.htm';

	/**
	 * @var db
	 */
	private $config;
	/**
	 * @var string
	 */
	private $root_path;
	/**
	 * @var string
	 */
	private $target_directory;

	public function __construct(
		db $config,
		string $root_path
	)
	{
		$this->config = $config;
		$this->root_path = $root_path;
		$this->set_target_directory();
	}

	public function get_target_directory(): string
	{
		return $this->target_directory;
	}

	public function get_image_upload_path(string $filename): string
	{
		return sprintf(
			'%s/%s/%s/%s',
			$this->root_path,
			$this->target_directory,
			$this->generate_path_for_image($filename),
			$filename
		);
	}

	public function get_image_path(string $filename): string
	{
		return sprintf(
			'%s/%s/%s',
			$this->target_directory,
			$this->generate_path_for_image($filename),
			$filename
		);
	}

	public function get_directories_path(string $filename): array
	{
		$filepath = $this->generate_path_for_image($filename);

		return explode('/', $filepath);
	}

	public function generate_index_htm_path(string $directory): string
	{
		return sprintf(
			'%s/%s/%s/%s',
			$this->root_path,
			$this->target_directory,
			$directory,
			self::INDEX_HTM_FILENAME
		);
	}

	public function generate_images_directory_path(string $path): string
	{
		return sprintf(
			'%s/%s',
			$this->root_path,
			$path
		);
	}

	private function generate_path_for_image(string $filename): string
	{
		return sprintf(
			'%s/%s',
			substr(bin2hex($filename), 0, 2),
			substr(bin2hex($filename), -2)
		);
	}

	private function set_target_directory(): void
	{
		$registered = $this->config->offsetGet(config::IMAGE_PATH_NAME);

		$this->target_directory = $registered ?: config::DEFAULT_IMAGE_PATH;
	}
}
