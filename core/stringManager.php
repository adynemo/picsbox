<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\core;


class stringManager
{
	/**
	 * @var pathManager
	 */
	private $path_manager;

	public function __construct(pathManager $path_manager)
	{
		$this->path_manager = $path_manager;
	}

	public function search_filenames(string $subject): array
	{
		$target_directory = $this->format_string_for_regex($this->path_manager->get_target_directory());

		$begin = '((<img\ssrc=")|(\[img[a-z]*\]))';
		$end = '((">)|(\[\/img[a-z]*\]))';
		$base_uri = 'https?:\/\/[a-z0-9.]{1,61}\.[a-z]{2,}';
		$sub_dir = '[0-9]{2}';
		$search = '[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}\.(jpeg|png)';

		$pattern = sprintf(
			'/%1$s%2$s%3$s\/%4$s\/%4$s\/(%5$s)%6$s/i',
			$begin,
			$base_uri,
			$target_directory,
			$sub_dir,
			$search,
			$end
		);

		preg_match_all($pattern, $subject, $matches);

		return $matches[4];
	}

	private function format_string_for_regex(string $subject): string
	{
		return str_replace(['/', '.'], ['\/', '\.'], $subject);
	}
}
