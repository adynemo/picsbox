<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\acp;

/**
 * @package module_install
 */
class acp_picsbox_info
{
	function module()
	{
		return [
			'filename' => '\ady\picsbox\acp\acp_picsbox_module',
			'title'    => 'ACP_CAT_PICSBOX',
			'modes'    => [
				'settings' => [
					'title' => 'ACP_PICSBOX_CONFIG',
					'auth'  => 'ext_ady/picsbox',
					'cat'   => ['ACP_CAT_PICSBOX'],
				],
			],
		];
	}
}
