<?php

/**
 *
 * @package   phpBB Extension - PicsBox
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\picsbox\acp;

use ady\picsbox\constant\config;
use ady\picsbox\core\adminManager;
use ady\picsbox\object\settings;
use phpbb\config\db;
use phpbb\request\request;
use phpbb\template\template;
use phpbb\user;

class acp_picsbox_module
{
	/**
	 * @var string
	 */
	public $u_action;
	/**
	 * @var string
	 */
	public $tpl_name;
	/**
	 * @var mixed
	 */
	public $page_title;
	/**
	 * @var user
	 */
	private $user;

	public function main($id, $mode)
	{
		/**
		 * @var user                   $user
		 * @var template               $template
		 * @var request                $request
		 * @var \phpbb_cache_container $phpbb_container
		 * @var db                     $config
		 */
		global $user, $template, $request, $phpbb_container, $config;

		$this->user = $user;

		/**
		 * @var adminManager $adminManager
		 */
		$adminManager = $phpbb_container->get('ady.picsbox.core.adminManager');
		$stats = $adminManager->calculateStats();

		$user->add_lang('acp/common');
		$user->add_lang_ext('ady/picsbox', 'acp/info_acp_picsbox');
		$this->tpl_name = 'acp_picsbox';
		$this->page_title = $user->lang['ACP_CAT_PICSBOX'];
		add_form_key('acp_picsbox');
		$allow_original_size = filter_var($config->offsetGet(config::ALLOW_ORIGINAL_SIZE_NAME), FILTER_VALIDATE_BOOLEAN);

		$is_purge_form = $request->variable('is_purge_form', false);
		$is_settings_form = $request->variable('is_settings_form', false);
		$do_process = $request->variable('picsbox_images_purge', false);

		if ($request->is_set_post('submit'))
		{
			if (!check_form_key('acp_picsbox'))
			{
				trigger_error('FORM_INVALID');
			}

			if (true === $is_settings_form)
			{
				$images_path = $request->variable('picsbox_image_path', '', true);
				$max_upload_size = $request->variable('picsbox_upload_size', 0, true);
				$allow_original_size = $request->variable('picsbox_original_size', 0);
				$settings = (new settings())
					->set_image_path($images_path)
					->set_max_upload_size($max_upload_size)
					->set_allow_original_size($allow_original_size);
				$result = $adminManager->processGeneralSettings($settings);

				$this->refreshForm(!$result);
			}
			elseif ((true === $is_purge_form) && (true === $do_process))
			{
				$result = $adminManager->processImagesPurge();

				$this->refreshForm(!$result);
			}
			else
			{
				$this->refreshForm(true);
			}
		}

		$template->assign_vars([
			'PICSBOX_IMAGE_PATH'          => $config->offsetGet(config::IMAGE_PATH_NAME),
			'PICSBOX_UPLOAD_MAX_SIZE'     => $config->offsetGet(config::UPLOAD_MAX_SIZE_NAME) ?: 0,
			'PICSBOX_ALLOW_ORIGINAL_SIZE' => $allow_original_size,
			'U_ACTION_SETTINGS'           => $this->u_action . '&is_settings_form=true',
			'U_ACTION_REMOVE'             => $this->u_action . '&is_purge_form=true',
			'PICSBOX_COUNT_IMAGES'        => $stats['count'],
			'PICSBOX_COUNT_ORPHANS'       => $stats['orphans'],
		]);
	}

	private function refreshForm(bool $is_error = false)
	{
		meta_refresh(3, $this->u_action);

		if ($is_error)
		{
			trigger_error($this->user->lang['ACP_PICSBOX_ERROR'] . adm_back_link($this->u_action), E_USER_WARNING);

			return;
		}

		trigger_error($this->user->lang['ACP_PICSBOX_SAVED'] . adm_back_link($this->u_action));
	}
}
