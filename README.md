# phpBB Extension - PicsBox

## Install

1. Download the [latest release](https://gitlab.com/adynemo/picsbox/-/releases).
2. Unzip the downloaded release, and change the name of the folder to `picsbox`.
3. In the `ext` directory of your phpBB board, create a new directory named `ady` if it does not already exist.
4. Copy the `picsbox` folder to `/ext/ady/`. If done correctly, the folder structure should look like
   this: `{your_forum_root}/ext/ady/picsbox/composer.json`.
5. Navigate in the ACP to `Customise -> Manage extensions`.
6. Look for `View Friends` under the `Disabled Extensions` list, and click its `Enable` link.

## Uninstall

1. Navigate in the ACP to `Customise -> Extension Management -> Extensions`.
2. Look for `PicsBox` under the `Enabled Extensions` list, and click its `Disable` link.
3. To permanently uninstall, click `Delete Data` and then delete the `/ext/ady/picsbox` folder.

## License

[GNU General Public License v2](http://opensource.org/licenses/GPL-2.0)
